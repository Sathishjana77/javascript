



//post method


var http = new XMLHttpRequest();
http.open("POST", "https://jsonplaceholder.typicode.com/posts");
http.setRequestHeader("content-type", "application/json; charset=UTF-8");
var requestBody = {
                        title: 'foo',
                        body: 'bar',
                        userId: 1
                        }
http.onreadystatechange= function(){ 
            var response = {}
if(this.status == 201 && this.readyState == 4){
                try{
                    response = JSON.parse(this.response) 
                }catch(exception){
                    console.log("Parsing issue: ", exception)
                }
                if(response != null && response !=""){
                    console.log(response.id);
                    var _div            = document.createElement('div')
                        _div.innerHTML  = response.id
                }
           }
          }

http.send(requestBody);






// put method



function sam() {
  var http = new XMLHttpRequest();
  http.open('PUT', 'https://jsonplaceholder.typicode.com/posts/33');
  http.onreadystatechange = function() {
  if(this.readyState === 4 && this.status === 200) {
  console.log(this.responseText);
  }
  }
  http.send({"id":33,
  "title":"flow"});
  
  
  }
  sam();


  //patch


  function sam() {
    var http = new XMLHttpRequest();
    http.open('PATCH', 'https://jsonplaceholder.typicode.com/posts/33');
    http.onreadystatechange = function() {
    if(this.readyState === 4 && this.status === 200) {
    console.log(this.responseText);
    }
    }
    http.send({"title":"flow"});
    
    
    }
    sam();




    //delete

    function sam() {
      var http = new XMLHttpRequest();
      http.open('DELETE','https://jsonplaceholder.typicode.com/posts/1');
      http.onreadystatechange = function() {
      if(this.readyState === 4 && this.status === 200) {
      console.log(this.responseText);
      }
      }
      http.send({"id":"1"});
      
      
      }
      sam();